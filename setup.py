from setuptools import setup, find_packages

setup(
    name='model-zendesk',
    version='0.2',
    description='Meltano .m5o models for data fetched using the Zendesk API',
    packages=find_packages(),
    package_data={'models': ['**/*.m5o', '*.m5o']},
    install_requires=[],
)
